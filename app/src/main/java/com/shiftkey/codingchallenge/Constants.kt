package com.shiftkey.codingchallenge

object Constants {
    const val BASE_URL = "https://staging-app.shiftkey.com/api/"
}