package com.shiftkey.codingchallenge

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.shiftkey.codingchallenge.domain.model.Shift
import com.shiftkey.codingchallenge.presentation.shiftdetail.ShiftDetailScreen
import com.shiftkey.codingchallenge.theme.ShiftsTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ShiftsTheme {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                title = {
                                    Text(text = "Shift details")
                                },
                                navigationIcon = {
                                    IconButton(
                                        onClick = { findNavController().navigateUp() }
                                    ) {
                                        Icon(
                                            imageVector = Icons.Default.ArrowBack,
                                            contentDescription = null
                                        )
                                    }
                                }
                            )
                        }
                    ) {
                        // TODO Add SafeArgs
                        val shiftItem = arguments?.getParcelable<Shift.ShiftItem>("shift")!!
                        ShiftDetailScreen(shiftItem = shiftItem)
                    }
                }
            }
        }
    }
}