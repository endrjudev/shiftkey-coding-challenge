package com.shiftkey.codingchallenge

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import com.shiftkey.codingchallenge.presentation.shiftlist.ShiftListScreen
import com.shiftkey.codingchallenge.presentation.shiftlist.ShiftListViewModel
import com.shiftkey.codingchallenge.theme.ShiftsTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ListFragment : Fragment() {

    private val viewModel: ShiftListViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                ShiftsTheme {
                    Scaffold(
                        topBar = {
                            TopAppBar(
                                title = {
                                    Text(text = "Shift list")
                                },
                            )
                        }
                    ) {
                        ShiftListScreen(viewModel = viewModel) { shift ->
                            findNavController().navigate(
                                resId = R.id.action_listFragment_to_detailFragment,
                                args = bundleOf("shift" to shift)
                            )
                        }
                    }
                }
            }
        }
    }
}