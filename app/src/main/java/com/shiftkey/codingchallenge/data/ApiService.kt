package com.shiftkey.codingchallenge.data

import com.shiftkey.codingchallenge.data.model.ShiftsResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("$API_VERSION/available_shifts")
    suspend fun availableShifts(
        @Query("start") startDate: String,
        @Query("address") address: String = DEFAULT_ADDRESS,
        @Query("radius") radius: Int = DEFAULT_RADIUS,
        @Query("type") type: String = DEFAULT_TYPE
    ): ShiftsResponse

    companion object {
        const val API_VERSION = "v2"

        const val DEFAULT_RADIUS = 150
        const val DEFAULT_ADDRESS = "Dallas, TX"
        const val DEFAULT_TYPE = "week"
    }
}