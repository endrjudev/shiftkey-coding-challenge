package com.shiftkey.codingchallenge.data

import okhttp3.Headers
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val contentTypeHeader = "Content-Type" to "application/json"
        val acceptHeader = "Accept" to "application/json"

        val headers = Headers.Builder()
            .add(contentTypeHeader.first, contentTypeHeader.second)
            .add(acceptHeader.first, acceptHeader.second)
            .build()

        var request = chain.request()

        request = request.newBuilder()
            .headers(headers)
            .build()

        return chain.proceed(request)
    }
}