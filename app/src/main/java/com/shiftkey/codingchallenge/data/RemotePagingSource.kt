package com.shiftkey.codingchallenge.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.shiftkey.codingchallenge.domain.model.Shift
import okio.IOException
import retrofit2.HttpException
import java.time.Duration
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class RemotePagingSource(
    private val apiService: ApiService,
    private val startDate: LocalDate
) : PagingSource<String, Shift>() {

    override fun getRefreshKey(state: PagingState<String, Shift>): String? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.localDate()?.plusWeeks(1)
                ?.stringDate()
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.localDate()?.minusWeeks(1)
                    ?.stringDate()
        }
    }

    override suspend fun load(params: LoadParams<String>): LoadResult<String, Shift> {
        val date = params.key ?: startDate.stringDate()

        return try {
            val response = apiService.availableShifts(date)
            val data = response.data
                .groupBy { it.date }
                .filter { it.value.flatMap { data -> data.shifts }.isNotEmpty() }
                .flatMap {
                    listOf(Shift.ShiftHeader(it.key)) +
                            it.value
                                .flatMap { data -> data.shifts }
                                .map { shift ->
                                    val offsetStartTime = shift.startTime.offsetDateTime()
                                    val offsetEndTime = shift.endTime.offsetDateTime()
                                    Shift.ShiftItem(
                                        id = shift.shiftId,
                                        type = shift.shiftKind,
                                        specialty = shift.localizedSpecialty.name,
                                        startTime = offsetStartTime.toLocalTime().toString(),
                                        endTime = offsetEndTime.toLocalTime().toString(),
                                        duration = Duration.between(
                                            offsetStartTime,
                                            offsetEndTime
                                        ).toHours()
                                    )
                                }
                }

            val nextDate = if (data.isEmpty())
                null
            else
                date.localDate().plusWeeks(1).stringDate()

            return LoadResult.Page(
                data = data,
                prevKey = null,
                nextKey = nextDate
            )
        } catch (exception: IOException) {
            LoadResult.Error(exception)
        } catch (exception: HttpException) {
            LoadResult.Error(exception)
        }
    }

    private fun LocalDate.stringDate(): String {
        return format(DateTimeFormatter.ISO_LOCAL_DATE)
    }

    private fun String.localDate(): LocalDate {
        return LocalDate.parse(this, DateTimeFormatter.ISO_LOCAL_DATE)
    }

    private fun String.offsetDateTime(): OffsetDateTime {
        return OffsetDateTime.parse(this, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
    }

}