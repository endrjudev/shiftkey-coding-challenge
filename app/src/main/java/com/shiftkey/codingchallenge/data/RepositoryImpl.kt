package com.shiftkey.codingchallenge.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.shiftkey.codingchallenge.domain.Repository
import com.shiftkey.codingchallenge.domain.model.Shift
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

class RepositoryImpl(private val service: ApiService) : Repository {

    override fun fetchAllShifts(startDate: LocalDate): Flow<PagingData<Shift>> {
        return Pager(
            config = PagingConfig(
                pageSize = DEFAULT_PAGE_SIZE,
                enablePlaceholders = false
            ),
            pagingSourceFactory = {
                RemotePagingSource(
                    apiService = service,
                    startDate = startDate
                )
            }
        ).flow
    }

    private companion object {
        const val DEFAULT_PAGE_SIZE = 20
    }
}