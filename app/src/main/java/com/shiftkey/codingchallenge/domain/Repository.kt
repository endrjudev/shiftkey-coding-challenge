package com.shiftkey.codingchallenge.domain

import androidx.paging.PagingData
import com.shiftkey.codingchallenge.domain.model.Shift
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate

interface Repository {
    fun fetchAllShifts(startDate: LocalDate): Flow<PagingData<Shift>>
}