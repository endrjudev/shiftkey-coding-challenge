package com.shiftkey.codingchallenge.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

// TODO Add more fields if necessary
sealed class Shift : Parcelable {

    @Parcelize
    data class ShiftItem(
        val id: Int,
        val type: String,
        val specialty: String,
        val startTime: String,
        val endTime: String,
        val duration: Long
    ) : Shift()

    @Parcelize
    data class ShiftHeader(
        val week: String
    ) : Shift()
}