package com.shiftkey.codingchallenge.domain.usecase

import androidx.paging.PagingData
import com.shiftkey.codingchallenge.domain.Repository
import com.shiftkey.codingchallenge.domain.model.Shift
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import java.time.LocalDate
import javax.inject.Inject

class FetchAllShiftsUseCase @Inject constructor(
    private val repository: Repository
) {

    operator fun invoke(startDate: LocalDate): Flow<PagingData<Shift>> {
        return repository.fetchAllShifts(startDate = startDate)
            .flowOn(Dispatchers.IO)
    }
}