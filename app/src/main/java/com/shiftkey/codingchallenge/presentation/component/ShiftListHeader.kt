package com.shiftkey.codingchallenge.presentation.component

import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp
import com.shiftkey.codingchallenge.domain.model.Shift

@Composable
fun ShiftListHeader(modifier: Modifier = Modifier, item: Shift.ShiftHeader) {
    Card(
        modifier = modifier.fillMaxSize(),
        backgroundColor = MaterialTheme.colors.primary,
        shape = RectangleShape
    ) {
        Row(modifier = Modifier.padding(8.dp)) {
            Text(text = "Shifts for:")
            Spacer(modifier = Modifier.requiredWidth(8.dp))
            Text(text = item.week)
        }
    }
}