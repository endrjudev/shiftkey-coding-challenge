package com.shiftkey.codingchallenge.presentation.component

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.shiftkey.codingchallenge.domain.model.Shift

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ShiftListItem(
    modifier: Modifier = Modifier,
    item: Shift.ShiftItem,
    onShiftClick: (Shift.ShiftItem) -> Unit
) {
    Card(
        modifier = modifier
            .fillMaxSize()
            .padding(horizontal = 8.dp),
        onClick = {
            onShiftClick(item)
        }
    ) {
        Row(Modifier.padding(8.dp)) {
            Column {
                BaseRow(label = "Type:", value = item.type)
                BaseRow(label = "Specialty:", value = item.specialty)
                BaseRow(label = "Starts at:", value = item.startTime)
                BaseRow(label = "Ends at:", value = item.endTime)
                BaseRow(label = "Duration:", value = item.duration.toString())
            }
        }
    }
}