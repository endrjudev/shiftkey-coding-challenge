package com.shiftkey.codingchallenge.presentation.shiftdetail

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import com.shiftkey.codingchallenge.domain.model.Shift
import com.shiftkey.codingchallenge.presentation.component.BaseRow

@Composable
fun ShiftDetailScreen(shiftItem: Shift.ShiftItem) {

    Column {
        BaseRow(label = "Type:", value = shiftItem.type)
        BaseRow(label = "Specialty:", value = shiftItem.specialty)
        BaseRow(label = "Starts at:", value = shiftItem.startTime)
        BaseRow(label = "Ends at:", value = shiftItem.endTime)
        BaseRow(label = "Duration:", value = shiftItem.duration.toString())
    }
}