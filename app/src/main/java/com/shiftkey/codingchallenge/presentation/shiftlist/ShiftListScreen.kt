package com.shiftkey.codingchallenge.presentation.shiftlist

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.items
import com.shiftkey.codingchallenge.domain.model.Shift
import com.shiftkey.codingchallenge.presentation.component.ShiftListHeader
import com.shiftkey.codingchallenge.presentation.component.ShiftListItem

@Composable
fun ShiftListScreen(viewModel: ShiftListViewModel, onShiftClick: (Shift.ShiftItem) -> Unit) {
    val shifts = viewModel.state.collectAsLazyPagingItems()

    when (shifts.loadState.refresh) {
        is LoadState.Loading -> ShowLoading()
        is LoadState.Error -> ShowError()
        is LoadState.NotLoading -> ShowContent(shifts = shifts, onShiftClick = onShiftClick)
    }
}

@Composable
fun ShowLoading() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
fun ShowError() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(text = "Error")
    }
}

@Composable
private fun ShowContent(
    shifts: LazyPagingItems<Shift>,
    onShiftClick: (Shift.ShiftItem) -> Unit
) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        items(shifts) { shift ->
            when (shift) {
                is Shift.ShiftHeader -> ShiftListHeader(item = shift)
                is Shift.ShiftItem -> ShiftListItem(item = shift, onShiftClick = onShiftClick)
                null -> return@items
            }
        }
    }
}