package com.shiftkey.codingchallenge.presentation.shiftlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.shiftkey.codingchallenge.domain.model.Shift
import com.shiftkey.codingchallenge.domain.usecase.FetchAllShiftsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import java.time.LocalDate
import javax.inject.Inject

@HiltViewModel
class ShiftListViewModel @Inject constructor(
    fetchAllShiftsUseCase: FetchAllShiftsUseCase
) : ViewModel() {

    val state: Flow<PagingData<Shift>> = fetchAllShiftsUseCase(LocalDate.now())
        .cachedIn(viewModelScope)
}