package com.shiftkey.codingchallenge.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable
fun ShiftsTheme(content: @Composable () -> Unit) {
    val lightColors = lightColors(
        primary = Color(0xff000000),
        primaryVariant = Color(0xff000000),
        secondary = Color(0xff797979),
        secondaryVariant = Color(0xff5c5c5c)

    )
    MaterialTheme(
        colors = lightColors
    ) {
        content()
    }
}